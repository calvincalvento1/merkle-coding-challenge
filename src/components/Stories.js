import React, { useState, useEffect } from 'react'
import '../css/stories.scss'
import HackerProfile from '../assets/hacker-profile.jpg'
import moment from 'moment/moment'
const Stories = () => {
    const [topStoryList, setTopStoryList] = useState([])

    async function getStoryList() {
        let response = await fetch("https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty");
        let result = await response.json();
        const limitStoryResult = result.slice(0, 10)
        const storyListDatas = await Promise.all(limitStoryResult.map((item) => {
            return getStoriesData(item)
        }));
        setTopStoryList(storyListDatas.sort((a, b) => (a.score > b.score) ? 1 : -1)
        );

    }

    async function getStoriesData(id) {
        const response = await fetch(`https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`);
        if (response.ok) {
            const result = await response.json()
            result.user = await getStoriesUserData(result.by)
            return result
        }
        else {
            console.log(response)
        }
    }

    async function getStoriesUserData(id) {

        let response = await fetch(`https://hacker-news.firebaseio.com/v0/user/${id}.json?print=pretty`);
        let result = await response.json();
        return result
    }

    useEffect(() => {
        getStoryList()
    }, [])

    return (
        <div className='stories'>
            <h1>Popular Stories</h1>
            <hr></hr>
            <div className="stories-blog-container">
                {topStoryList.map(item => {
                    return (
                        <div className='stories-blog-item'>
                            <img style={{ width: "100%" }} src={HackerProfile} alt="" />
                            <p>{item.by}</p>
                            <h2>{item.title}</h2>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                            <p><span style={{ fontWeight: "bold" }}>Score: </span>{item.score}</p>
                            <p><span style={{ fontWeight: "bold" }}>Karma Score: </span>{item.user.karma}</p>
                            <p><span style={{ fontWeight: "bold" }}>Published at: </span>{moment.unix(item.time).format("MM/DD/YYYY")}</p>
                            <br />
                            <a href={item.url} style={{ fontWeight: "bold" }} target="blank">Read More -></a>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

export default Stories