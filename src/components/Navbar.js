import React from 'react'
import "../css/navbar.scss"
export const Navbar = () => {
    return (
        <div className='parent'>
            <div className='navbar'>
                <h1>HackerStories</h1>
                <ul className='right-panel'>
                    <li>SOLUTIONS</li>
                    <li>PRODUCTS</li>
                    <li>PARTNERS</li>
                    <li>COMPANY</li>
                    <li>HACKERS</li>
                    <li>RESOURCES</li>
                </ul>
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="hamburger">
                    <path strokeLinecap="round" strokeLinejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
                </svg>
            </div>
        </div>
    )
}
