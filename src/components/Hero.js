import React from 'react'
import '../css/hero.scss'
import HackerLogo from '../assets/hackerlogo.jpg'
const Hero = () => {
    return (
        <div className='hero'>
            <p>Home > <span style={{ fontWeight: "bold" }}>HackerStories</span></p>
            <h1>HackerStories Blog</h1>
            <p><span style={{ fontWeight: "bold" }}>Lorem Ipsum</span> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            <div className='intro'>
                <div className='item'>
                    <img src={HackerLogo} alt="" />
                </div>
                <div className='item '>
                    <div className='item2'>
                        <p>VULNERABILITY MANAGEMENT, COMPANY NEWS</p>
                        <h1>Introducing HackerStories Assets</h1>
                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."</p>

                    </div>
                </div>
            </div>
        </div >
    )
}

export default Hero