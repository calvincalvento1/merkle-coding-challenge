import './App.css';
import { Navbar } from './components/Navbar';
import Hero from './components/Hero';
import Stories from './components/Stories';
import Footer from './components/Footer';

function App() {
  return (
    <div className="App">
      <Navbar />
      <Hero />
      <Stories />
      <Footer />
    </div>
  );
}

export default App;
